//
//  BaseCollectionViewCell.swift
//  CircleControl
//
//  Created by Lera Mozgovaya on 3/5/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

class BaseCollectionViewCell : UICollectionViewCell {
    
    static func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        
        let nib = UINib(nibName: String(describing: self), bundle: nil)
        
        collectionView.register(nib, forCellWithReuseIdentifier: String(describing: self))
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: self), for: indexPath)
    }
}
