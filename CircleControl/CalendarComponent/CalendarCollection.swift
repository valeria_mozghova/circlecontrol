//
//  CalendarCollection.swift
//  CircleControl
//
//  Created by Lera Mozgovaya on 3/5/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

enum CalendarCollectionState {
    case calendar
    case chart
}

class WeekItem {
    var calendarChartData: [CalendarCollectionState : [DayChartItem]] = [:]
    var calendarPlainData: [CalendarCollectionState : [DayItem]] = [:]
}

class DayChartItem {
    
}

class DayItem {
    
}

class CalendarCollection: UIView {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var weekItem = WeekItem()
    
    var calendarCollectionState: CalendarCollectionState = .calendar {
        didSet {
            collectionView.reloadData()
        }
    }
}

extension CalendarCollection: UICollectionViewDelegate {
    
}

extension CalendarCollection: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return UICollectionViewCell()
    }

}
