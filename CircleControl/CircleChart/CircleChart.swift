//
//  CircleChart.swift
//  CircleControl
//
//  Created by Lera Mozgovaya on 3/4/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

private class Slice {
    
    var value: CGFloat = 0
    var color: UIColor = .blue
    
    var isFilled: Bool = false {
        didSet {
            color = isFilled ? .green : .gray
        }
    }
}

@IBDesignable open class CircleChart: UIView {

    @IBInspectable open var counter: Int = 8 {
        didSet {
            slices.removeAll()
                for _ in 0 ... counter - 1 {
                    let slice = Slice()
                    slices.append(slice)
                }
        }
    }
    
    @IBInspectable open var arcWidth: CGFloat = 17
    @IBInspectable open var spaceBeetwenSectors: CGFloat = 1
    
    public var total: CGFloat = 0
    
    public var finishedPercentClosure: ((CGFloat) -> Void)?
    
    private var slices: [Slice] = Array(repeating: Slice(), count: 10)
    public var sliceValues: [(CGFloat, UIColor)] = [] {
        
        didSet {
            counter = sliceValues.count
            slices.removeAll()
            
            for sv in sliceValues {
                let slice = Slice()
                slice.color = sv.1
                slice.value = sv.0
                slices.append(slice)
            }
        }
    }
    
    open override func draw(_ rect: CGRect) {
        
        let outterRadius: CGFloat = min(bounds.width, bounds.height) / 2
        let innerRadius: CGFloat = outterRadius - arcWidth
        
        let commonValue = slices.reduce(0) { (res, slice) -> CGFloat in
            slice.value + res
        }
        
        print(commonValue)
        
        let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        var startValue: CGFloat = 0
        var startAngle: CGFloat = 0
        var endValue: CGFloat = 0
        var endAngle: CGFloat = 0
        
        let widthPerAngle = .pi * 2 / CGFloat(slices.count)
        
        for i in 0...slices.count - 1 {
            
            let slice = slices[i]
            
            if commonValue > 0 {
                
                // Для ресайза по процентам
                startAngle = (startValue * 2 * .pi) - .pi / 2
                endValue = startValue + (slice.value / commonValue)
                endAngle = ((endValue * 2 * .pi) - .pi / 2)
                
                let persentage = CGFloat(commonValue * 100) / CGFloat(total)
                finishedPercentClosure?(persentage)
            }
            else {
                // Для ресайза по колисечтву

                startAngle = widthPerAngle * CGFloat(i) - .pi / 2
                endAngle = startAngle + widthPerAngle
                
                let filledCount = slices.filter({$0.isFilled}).count
                let persentage = CGFloat(filledCount * 100) / CGFloat(slices.count)
                finishedPercentClosure?(persentage)
            }
            
            let path = UIBezierPath()
            
            path.move(to: center)
            path.addArc(withCenter: center, radius: outterRadius, startAngle: startAngle + spaceBeetwenSectors.degreesToRadians, endAngle: endAngle, clockwise: true)

            slice.color.setFill()
            path.fill()
            // increase start value for next slice
            startValue += slice.value / commonValue
        }
        
        // create center donut hole
        let innerPath = UIBezierPath()
        innerPath.move(to: center)
        innerPath.addArc(withCenter: center, radius: innerRadius, startAngle: 0, endAngle: CGFloat(Double.pi) * 2, clockwise: true)
        UIColor.white.setFill()
        innerPath.fill()
        innerPath.close()
        
//        finishedPercentClosure?()
    }
    
    open func updateFilledState(isFilled: Bool, forSliceAt idx: Int) {
        
        let slice = slices[idx]
        slice.isFilled = isFilled
        
        setNeedsDisplay()
    }
    
    func updateValue(_ value: CGFloat, forSliceAt idx: Int) {
        
        let slice = slices[idx]
        slice.value = value
        
        setNeedsDisplay()
    }
}
