//
//  Ring.swift
//  CircleControl
//
//  Created by Lera Mozgovaya on 2/26/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit
import AudioToolbox.AudioServices

fileprivate class SectorElement {
    
    var bezierPath: UIBezierPath
    var color: UIColor = .black
    var textMiddleAngle: CGFloat = 0
    var isSelected: Bool = false
    var sectorText: String = ""
    
    init(bezierPath: UIBezierPath) {
        self.bezierPath = bezierPath
    }
}

@IBDesignable open class RingView: UIView {
    
    //MARK: - Customizable public components
    @IBInspectable open var counter: Int = 5
    @IBInspectable open var selectedSectorColor: UIColor = .white
    @IBInspectable open var sectorColor: UIColor = .black
    @IBInspectable open var arcWidth: CGFloat = 100
    @IBInspectable open var shouldVibrateOnTap: Bool = false
    @IBInspectable open var shouldVibrateOnSwipe: Bool = true
    @IBInspectable open var spaceBeetwenSectors: CGFloat = 0.5
    @IBInspectable open var radius: CGFloat = 100
    
    open var selectedElementClosure: ((Int) -> Void)?
    
    open var textFont: UIFont = UIFont.systemFont(ofSize: 13)
    
    open var buttonTitles: [String] = [] {
        didSet {
            if buttonTitles.count > 0 {
                counter = buttonTitles.count
            }
        }
    }
    
    open func reloadView() {
        
        textFont = UIFont.systemFont(ofSize: 13)
        sectorElements.removeAll()
        currentEl = nil
        setNeedsDisplay()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private var sectorElements = [SectorElement]()
    
    override open func draw(_ rect: CGRect) {
        
        let center = CGPoint(x: bounds.width, y: bounds.height)
        let startAngle: CGFloat = .pi
        let endAngle: CGFloat =  .pi / 2
    
        func createButtons() {
            
            for i in 0...counter - 1 {
            
                let angleDifference: CGFloat = 2 * .pi - (startAngle + endAngle)
                
                //then calculate the arc for each single button
                let arcLengthPerGlass = abs(angleDifference) / CGFloat(counter)
              
                let littleSectorStart = arcLengthPerGlass * CGFloat(i) + .pi
                //then multiply out by the actual glasses drunk
                let outlineEndAngle = littleSectorStart + arcLengthPerGlass
                
                let sectorPath = UIBezierPath(arcCenter: center,
                                              radius: radius/2 - arcWidth/2,
                                              startAngle: littleSectorStart + spaceBeetwenSectors.degreesToRadians,
                                              endAngle: outlineEndAngle,
                                              clockwise: true)
                
                let tmp: CGPath =  sectorPath.cgPath.copy(strokingWithWidth: arcWidth,
                                                   lineCap: CGLineCap(rawValue: 0)!,
                                                   lineJoin: CGLineJoin(rawValue: 0)!,
                                                   miterLimit: 1)
                
                let new = UIBezierPath( cgPath: tmp )
                
                let sectorElement = SectorElement(bezierPath: new)

                sectorElement.color = sectorColor
                
                sectorElement.color.setFill()
                sectorElement.bezierPath.fill()
                
                
                if buttonTitles.count == counter {
                    sectorElement.sectorText = buttonTitles[i]
                }
            
                let middleAngle = (littleSectorStart + outlineEndAngle) / 2
                
                sectorElement.textMiddleAngle = middleAngle
                sectorElements.append(sectorElement)

                drawRotatedText(sectorElement.sectorText.uppercased(), at: CGPoint(x: sectorElement.bezierPath.bounds.midX, y: sectorElement.bezierPath.bounds.midY), angle: (middleAngle - .pi), font: textFont, color: .white, delta: CGFloat(i), pathRect: sectorPath.bounds)
            }
        }
        
        if sectorElements.count == 0 {
            createButtons()
        }
        else {
            for (i, el) in sectorElements.enumerated() {
                
                el.color.setFill()
                el.bezierPath.fill()
                
                drawRotatedText(el.sectorText.uppercased(), at: CGPoint(x: el.bezierPath.bounds.midX, y: el.bezierPath.bounds.midY), angle: (el.textMiddleAngle - .pi), font: textFont, color: .white, delta: CGFloat(i), pathRect: el.bezierPath.bounds)
            }
        }
    }
    
    func drawRotatedText(_ text: String, at p: CGPoint, angle: CGFloat, font: UIFont, color: UIColor, delta: CGFloat, pathRect: CGRect) {
        // Draw text centered on the point, rotated by an angle in degrees moving clockwise.
        let attrs = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color]
        let textSize = text.size(withAttributes: attrs)
        let c = UIGraphicsGetCurrentContext()!
        c.saveGState()
        // Translate the origin to the drawing location and rotate the coordinate system.

        c.translateBy(x: p.x, y: p.y)
        c.rotate(by: angle)
        // Draw the text centered at the point.
        
        let i = delta + 1
        
        let yOffset = -textSize.height / i * (i - 0.9)

        text.draw(at: CGPoint(x: -textSize.width / 2, y: yOffset), withAttributes: attrs)
        
        // Restore the original coordinate system.
        c.restoreGState()
        c.setFillColor(UIColor.blue.cgColor)

    }

    private var dragging: Bool = false
    
    fileprivate var currentEl: SectorElement!
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        let touch = touches.first

        let point = touch?.location(in: self)

        dragging = true
        
        for (idx, element) in sectorElements.enumerated() {

            if element.bezierPath.contains(point!) {
                currentEl = element
                hangleActionForButton(idx, element: element)
                
                if shouldVibrateOnTap {
                    vibrate()
                }
            }
        }
    }
    
    override open func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        let point = touch?.location(in: self)
        
        if (dragging) {
            
            for (idx, element) in sectorElements.enumerated() {
               
                if element.bezierPath.contains(point!) && currentEl.bezierPath.contains(point!) == false {
                    
                    print(idx)
                    currentEl = element
                    hangleActionForButton(idx, element: element)
                    
                    if shouldVibrateOnSwipe {
                        vibrate()
                    }
                }
            }
        }
    }
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        dragging = false
    }
    
    fileprivate func hangleActionForButton(_ idx: Int, element: SectorElement) {
        
        sectorElements.forEach { (el) in
            if el.bezierPath != element.bezierPath {
                 el.isSelected = false
                 el.color = sectorColor
            }
        }
        
        element.isSelected.toggle()
        
        if element.isSelected {
            element.color = selectedSectorColor
        }
        else {
            element.color = sectorColor
        }
        
        if element.isSelected {
            selectedElementClosure?(idx)
        }
        
        self.setNeedsDisplay()
    }
    
    private func vibrate() {
        
        // UI "impact"
        UIImpactFeedbackGenerator(style: .medium).impactOccurred()
        
        // Selection changed
        UISelectionFeedbackGenerator().selectionChanged()
        
        // Notifications
        
        // To be complete, this is the vibration using AudioToolbox
        AudioServicesPlaySystemSoundWithCompletion(kSystemSoundID_Vibrate, nil)
    }

}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}
