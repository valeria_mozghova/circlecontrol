//
//  ViewController.swift
//  CircleControl
//
//  Created by Lera Mozgovaya on 2/26/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
        
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var cicleChart: CircleChartView!
    
    @IBOutlet weak var circleChartMiddle: CircleChart! {
        didSet {
            circleChartMiddle.sliceValues = [(CGFloat(20.0), UIColor.random),(CGFloat(15.0), UIColor.random),(CGFloat(3.0), UIColor.random),(CGFloat(34.0), UIColor.random),(CGFloat(11.0), UIColor.random),(CGFloat(1.0), UIColor.random)]
        }
    }


    @IBOutlet weak var percentLabel: UILabel!
    
    var i = 0
    var currentQuestion = 0
    var clockwise = true
    
    var arr = [["very much \n unlike me", "\n unlike me", "neutral", "like me", "very much \n like me"]]
    
    @IBOutlet weak var ringView: RingView! {
        didSet {
            ringView?.selectedElementClosure = { [weak self] idx in
                print("path idx: \(idx)")
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ringView.buttonTitles = arr[i]
    
    }
    
    @IBAction func didClickNext(_ sender: UIButton) {
        
        if i < arr.count - 1 {
            i += 1
        }
        else {
            i = 0
        }
        
        ringView.buttonTitles = arr[i]
        ringView.reloadView()
        
        cicleChart.updateValues(isFilled: clockwise, forSliceAt: currentQuestion)
        
        if currentQuestion >= 7 {
            currentQuestion -= 1
        }
        else {
            currentQuestion += 1
        }
    }
}


extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
