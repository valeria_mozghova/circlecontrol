//
//  CircleChartView.swift
//  CircleControl
//
//  Created by Lera Mozgovaya on 3/5/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

class CircleChartView: UIView {

    @IBOutlet weak var circleChart: CircleChart! {
        didSet {
            
//            circleChart.sliceValues = [(CGFloat(20.0), UIColor.random),(CGFloat(15.0), UIColor.random),(CGFloat(3.0), UIColor.random),(CGFloat(34.0), UIColor.random),(CGFloat(11.0), UIColor.random),(CGFloat(1.0), UIColor.random)]
        
            circleChart.finishedPercentClosure = { [weak self] persent in
                self?.percentValueLabel?.text = "\(persent)"
            }
        }
    }
    
    @IBOutlet weak var topBadgeIndicator: UIImageView! {
        didSet {
            topBadgeIndicator?.isHidden = true
        }
    }
    @IBOutlet weak var percentValueLabel: UILabel!
    @IBOutlet weak var percentSignLabel: UILabel!
    @IBOutlet weak var finishedLabel: UILabel!
    @IBOutlet var contentView: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commontInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commontInit()
    }
    
    private func commontInit() {
        Bundle.main.loadNibNamed(String(describing: CircleChartView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func updateValues(isFilled: Bool, forSliceAt idx: Int) {
        circleChart.updateFilledState(isFilled: isFilled, forSliceAt: idx)
    }
}
