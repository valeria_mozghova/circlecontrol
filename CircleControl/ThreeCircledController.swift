//
//  ThreeCircledController.swift
//  CircleControl
//
//  Created by Lera Mozgovaya on 3/5/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

class ThreeCircledController: UIViewController {

    
    @IBOutlet weak var socialSlider: UISlider!
    @IBOutlet weak var mentalSlider: UISlider!
    @IBOutlet weak var physicalSlider: UISlider!
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var circleChart: CircleChart!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInitialState()
    }
    
    @IBAction func sliderDidChangeValue(_ sender: UISlider) {
        
        circleChart.updateValue( CGFloat(sender.value), forSliceAt: sender.tag)
    }
    
    private func configureInitialState() {
        
        circleChart.sliceValues = [(CGFloat(socialSlider.value), UIColor.blue),(CGFloat(mentalSlider.value), UIColor.magenta),(CGFloat(physicalSlider.value), UIColor.green)]
        
        circleChart.total = 720
        
        circleChart.finishedPercentClosure = { [weak self] persent in
            
            let roundedVal = round(persent)
            
            self?.countLabel?.text = "\(roundedVal)"
        }
    }
}
